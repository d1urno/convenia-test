<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Arroz',
            'price' => 4,
            'client_id' => 1,
        ]);
        DB::table('products')->insert([
            'name' => 'Frango',
            'price' => 8,
            'client_id' => 1
        ]);
        DB::table('products')->insert([
            'name' => 'Feijao',
            'price' => 5,
            'client_id' => 1
        ]);
        DB::table('products')->insert([
            'name' => 'Pure',
            'price' => 2,
            'client_id' => 11
        ]);
        DB::table('products')->insert([
            'name' => 'Batata',
            'price' => 4,
            'client_id' => 11
        ]);
        DB::table('products')->insert([
            'name' => 'Costella',
            'price' => 9,
            'client_id' => 11
        ]);
        DB::table('products')->insert([
            'name' => 'Mamao',
            'price' => 2,
            'client_id' => 21
        ]);
        DB::table('products')->insert([
            'name' => 'Zuco',
            'price' => 6,
            'client_id' => 21
        ]);
        DB::table('products')->insert([
            'name' => 'Costella',
            'price' => 9,
            'client_id' => 31
        ]);
        DB::table('products')->insert([
            'name' => 'Zuco',
            'price' => 6,
            'client_id' => 31
        ]);
    }
}
