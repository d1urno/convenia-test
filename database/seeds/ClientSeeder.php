<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clients')->insert([
            'table_id' => 1,
            'name' => 'Andre',
            'paid' => true
        ]);
        DB::table('clients')->insert([
            'table_id' => 1,
            'name' => 'Maria',
            'paid' => false
        ]);
        DB::table('clients')->insert([
            'table_id' => 11,
            'name' => 'Suzana',
            'paid' => false
        ]);
        DB::table('clients')->insert([
            'table_id' => 11,
            'name' => 'Luis',
            'paid' => false
        ]);
    }
}
