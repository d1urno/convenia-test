Feature: Individual payments, we have:
    table "a" with client "Andre" and client "Maria"
    table "b" with client "Suzana" and client "Luis"

    Scenario: A new Client joins a table
        Given Client "Junior" wants to join table "a"
        When I request "/api/clients" using "POST"
        Then the response status code should be 201

    Scenario: The new client orders its meal
        Given "Junior" wants to order "Frango" with price "8"
        When I request "/api/products" using "POST"
        Then the response status code should be 201

    Scenario: Clients on table "a" want to know their final charge
        When I request "/api/tables/a" using "GET"
        Then the response status code should be 200
        And the response should be JSON
        And the response has a "Total" property
        And the response has a "Produtos" property

    Scenario: Client has to go early
        Given Client "Andre" on table "a" wants to pay its products
        When I request "/api/clients/andre" using "PATCH"
        Then the response status code should be 201

    Scenario: Clients on table "a" want to know their individual charges and remaining payment
        When I request "/api/partials/a" using "GET"
        Then the response status code should be 200
        And the response should be JSON
        And the response has a "Pagamentos Parciales" property
        And the response has a "Pagamento Restante" property
