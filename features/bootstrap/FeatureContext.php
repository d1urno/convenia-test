<?php

use Behat\Behat\Context\Context;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use PHPUnit\Framework\Assert;

/**
 * Defines application features from the specific context.
 */
class FeatureContext implements Context
{

    public $res;
    public $client;
    public $payload;

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
        $base_uri = 'http://localhost:8000/';
        $this->client = new Client(['base_uri' => $base_uri]);
        $this->payload = [];
    }

    /**
     * @When I request :arg1 using :arg2
     * @param $arg1
     * @param $arg2
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function iRequest($arg1, $arg2)
    {
        $this->res = $this->client->request($arg2, $arg1, [
            'form_params' => $this->payload
        ]);
    }

    /**
     * @Then the response should be JSON
     */
    public function theResponseShouldBeJson()
    {
        $data = json_decode($this->res->getBody(true));
        Assert::assertNotNull($data);
    }

    /**
     * @Then the response status code should be :arg1
     * @param $arg1
     */
    public function theResponseStatusCodeShouldBe($arg1)
    {
        Assert::assertSame($arg1, (string)$this->res->getStatusCode());
    }

    /**
     * @Then the response has a :arg1 property
     * @param $arg1
     */
    public function theResponseHasAProperty($arg1)
    {
        $res_array = (array)json_decode($this->res->getBody(true));
        $data_array = (array)$res_array['data'];
        Assert::assertArrayHasKey($arg1, $data_array);
    }

    /**
     * @Given Client :arg1 on table :arg2 wants to pay its products
     * @param $arg1
     * @param $arg2
     */
    public function clientOnTableWantsToPayItsProducts($arg1, $arg2)
    {
        $this->payload = [
            'paid' => 1
        ];
    }

    /**
     * @Given Client :arg1 wants to join table :arg2
     * @param $arg1
     * @param $arg2
     */
    public function clientWantsToJoinTable($arg1, $arg2)
    {
        $this->payload = [
            'name' => $arg1,
            'table' => $arg2
        ];
    }

    /**
     * @Given :arg1 wants to order :arg2 with price :arg3
     * @param $arg1
     * @param $arg2
     * @param $arg3
     */
    public function wantsToOrder($arg1, $arg2, $arg3)
    {
        $this->payload = [
            'client' => $arg1,
            'name' => $arg2,
            'price' => $arg3
        ];
    }
}
