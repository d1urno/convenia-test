# Convenia-test

Adjunto link a API rodando em Heroku, para fazer provas direitas:

https://convenia-test.herokuapp.com

### Prova direita de exemplo (Postman)

1: Olhar os consumos e costo da mesa “a”:

-	GET -> http://convenia-test.herokuapp.com/api/tables/a

2: Cadastrar novo cliente “Matias” na mesa “a”:

-	POST -> http://convenia-test.herokuapp.com/api/clients?name=Matias&table=a

3: Olhar os pagamentos parciales e o restante da mesa “a”:

-	GET -> http://convenia-test.herokuapp.com/api/partials/a

4: Cadastrar pedido de “Pizza”do “Matias” com precio “13”:

-	POST -> http://convenia-test.herokuapp.com/api/products?client=matias&name=Pizza&price=13

5: “Matias” paga individualmente:

-	PATCH -> http://convenia-test.herokuapp.com/api/clients/matias?paid=1

6: Olhar os pagamentos parciales e o restante da mesa “a” novamente

-	GET -> http://convenia-test.herokuapp.com/api/partials/a
-------------------------------------------------------------------


