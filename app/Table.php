<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    public function clients()
    {
        return $this->hasMany('App\Client');
    }

    public function getTotalPrice()
    {
        $clients = $this->clients;
        $total = 0;
        foreach ($clients as $client) {
            $total += $client->getTotal();
        }
        return $total;
    }

    public function getProducts()
    {
        $clients = $this->clients;
        $response = [];
        foreach ($clients as $client) {
            $products = $client->products;
            foreach ($products as $product) {
                $response[] = [$product->name => $product->price];
            }
        }
        return $response;
    }

    public function getPartials()
    {
        $clients = $this->clients;
        $response = [];
        foreach ($clients as $client) {
            $response[] = ['Cliente ' . $client->name => $client->paid ? $client->getTotal() : 0];
        }
        return $response;
    }

    public function getRemaining()
    {
        $clients = $this->clients;
        $remaining = 0;
        foreach ($clients as $client) {
            $remaining += $client->paid ? 0 : $client->getTotal();
        }
        return $remaining;
    }
}
