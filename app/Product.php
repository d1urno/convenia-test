<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'price', 'client_id'];
    /**
     * Save a new model and return the instance.
     *
     * @param  array $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        $product = new Product([
            'name' => $attributes['name'],
            'price' => $attributes['price'],
            'client_id' => $attributes['client_id']
        ]);
        $product->save();
        return $product;
    }
}
