<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'table_id'];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function table()
    {
        return $this->hasOne('App\Table');
    }

    public function getTotal()
    {
        $products = $this->products();
        return $products->sum('price');
    }

    /**
     * Save a new model and return the instance.
     *
     * @param  array $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        $client = new Client([
            'name' => $attributes['name'],
            'table_id' => $attributes['table_id']
        ]);
        $client->save();
        return $client;
    }

}
