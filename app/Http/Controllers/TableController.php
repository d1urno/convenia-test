<?php

namespace App\Http\Controllers;

use App\Http\Resources\PartialsResource;
use App\Http\Resources\TableResource;
use App\Table;
use Illuminate\Http\Request;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  $table_name
     * @return TableResource
     */
    public function show($table_name)
    {
        $table = Table::where('name', '=', $table_name)->first();
        if (!$table) {
            return response('Tabela não encontrada', 400);
        }
        return new TableResource($table);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function edit(Table $table)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Table $table)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Table $table)
    {
        //
    }

    /**
     * Display partial payments.
     *
     * @param  $table_name
     * @return PartialsResource
     */
    public function partials($table_name)
    {
        $table = Table::where('name', '=', $table_name)->first();
        if (!$table) {
            return response('Tabela não encontrada', 400);
        }
        return new PartialsResource($table);
    }
}
