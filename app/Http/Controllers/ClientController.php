<?php

namespace App\Http\Controllers;

use App\Client;
use App\Table;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $table = Table::where('name', '=', $request['table'])->first();
        $rules = array(
            'name' => 'required',
            'table' => 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !$table) {
            return response('Entrada errada', 400);
        } else {
            Client::create([
                'name' => $request['name'],
                'table_id' => $table->id
            ]);
            return response('Success', 201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  $name
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $name)
    {
        $client = Client::where('name', '=', $name)->first();
        $rules = array(
            'paid' => 'required|boolean'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails() || !$client) {
            return response('Entrada errada', 400);
        } else {
            $client->paid = $request['paid'];
            $client->save();
            return response('Success', 201);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}
